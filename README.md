<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.mkinitcpio

A role to configure
[mkinitcpio](https://wiki.archlinux.org/index.php/Mkinitcpio) and its presets
and to generate the initrd image from them.

## Requirements

An Arch Linux system, that uses mkinitcpio to generate the initrd images or a
Linux host, that can chroot into an Arch Linux system, that uses mkinitcpio to
generate the initrd images.

## Role Variables

This role relies on the following variables to configure `mkinitcpio.conf` and
the `<kernel_name>.preset>` files:

* `mkinitcpio`: a dict to configure mkinitcpio (defaults to `None`)
  * `binaries`: a list of strings defining binaries to include in the resulting initrd (defaults to `[]`)
  * `compression`: a string defining the compression to use - one of `bzip2`, `cat`, `gzip`, `lz4`, `lzma`, `lzop`, `xz`, `zstd` (defaults to unset, which selects the current default)
  * `compression_options`: a list of strings passed to the compression tool as options (defaults to `[]`)
  * `files`: a list of absolute paths defining files to be included in the resulting initrd (defaults to `[]`)
  * `hooks`: a list of strings defining the hooks to be run when creating the initrd (defaults to `['base', 'udev', 'autodetect', 'modconf', 'kms', 'keyboard', 'keymap', 'consolefont', 'block', 'filesystems', 'fsck']`)
  * `modules`: a list of strings defining kernel modules to be included in the resulting initrd (defaults to `[]`)
  * `modules_decompress`: a boolean value indicating whether to decompress kernel modules during initramfs creation (defaults to `true`)
  * `presets`: a list of strings defining the presets to run (defaults to `['default']`)


The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbook

```yaml
- name: Create default initrd images in a chroot system
  hosts: hosts
  vars:
    chroot_dir: /mnt
    mkinitcpio:
      presets:
        - default
  tasks:
    - name: Include role using chroot tasks and handlers
      ansible.builtin.include_role:
        name: archlinux.mkinitcpio
        handlers_from: chroot
        tasks_from: chroot
```

```yaml
- name: Create only the default image in a system
  hosts: some_hosts_in_chroot
  vars:
    mkinitcpio:
      presets:
        - default
  tasks:
    - name: Include role using chroot tasks and handlers
      ansible.builtin.include_role:
        name: archlinux.mkinitcpio
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
